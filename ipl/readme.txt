Match_id 503
https://www.google.com/search?q=rcb+vs+srh+2014&sa=X&ved=2ahUKEwijgr2Si8zjAhUFO48KHSfSCJUQ1QIoAHoECBUQAQ&cshid=1563921009925295&biw=1536&bih=731#sie=m;/g/11f54h_6f7;5;/m/03b_lm1;cms;fp;1;;
https://www.google.com/search?q=rcb+vs+srh+2014&sa=X&ved=2ahUKEwijgr2Si8zjAhUFO48KHSfSCJUQ1QIoAHoECBUQAQ&cshid=1563921009925295&biw=1536&bih=731#sie=m;/g/11f54h_6f7;5;/m/03b_lm1;cms;fp;1;;

hatrick
37	1	Royal Challengers Bangalore	Mumbai Indians	20	4	P Negi	KM Jadhav	MJ McClenaghan
37	1	Royal Challengers Bangalore	Mumbai Indians	20	5	KM Jadhav	AF Milne	MJ McClenaghan
37	1	Royal Challengers Bangalore	Mumbai Indians	20	6	AF Milne	S Aravind	MJ McClenaghan

 The output of a correlated scalar subquery must be aggregated;
 .config("spark.driver.host","localhost")
 
 Over snapshot = Self join with <=, >= and serves as example for showing running_count and running_wkts. (non-equijoin)
 Hattrick = match_id=11 or match_id=110  or match_id=19
 
 /*==============ORACLE TABLE CREATION===========================*/
 
  CREATE TABLE ipl (
 	match_id  integer ,
 	inning  integer ,
 	batting_team  char(100) ,
 	bowling_team  char(100) ,
 	over  integer ,
 	ball  integer ,
 	batsman  char(50) ,
 	non_striker  char(50) ,
 	bowler  char(50) ,
 	is_super_over  integer ,
 	wide_runs  integer ,
 	bye_runs  integer ,
 	legbye_runs  integer ,
 	noball_runs  integer ,
 	penalty_runs  integer ,
 	batsman_runs  integer ,
 	extra_runs  integer ,
 	total_runs  integer ,
 	player_dismissed  char(50) ,
 	dismissal_kind  char(50) ,
 	fielder  char(50) 
 
 ) -- give upto this and load it using SQL developer
 ORGANIZATION EXTERNAL (
   TYPE ORACLE_LOADER
   ACCESS PARAMETERS (
     RECORDS DELIMITED BY NEWLINE
     FIELDS TERMINATED BY ','
     MISSING FIELD VALUES ARE NULL
   )
   LOCATION ('C:\Users\arul\Dropbox\data\ipl\deliveries.csv')
 )
REJECT LIMIT UNLIMITED;

 /* HIghest score of a team */
 
 with t1 as (            
             select match_id, batting_team, sum(total_runs) over(partition by match_id, batting_team) runs from ipl )
             select match_id, batting_team, runs from t1 where runs = ( select max(runs) from t1)
            group by match_id, batting_team, runs

-- Top 10 highest scoring teams in IPL            

with t1 as (            
            select match_id, batting_team, sum(total_runs) over(partition by match_id, batting_team) runs from ipl ),
            t2 as ( select distinct  match_id, batting_team, runs from t1 ),
            t3 as ( select match_id, batting_team, runs, row_number() over(order by runs desc) rw1 from t2) 
            select * from t3 where rw1<=10

-- Partnership

		with t1 as (            
			    select batsman,non_striker, max(cast(over||'.'||ball as decimal(7,3))) ball2, sum(total_runs) runs from ipl where match_id=503 and inning=1 group by batsman, non_striker),
			  t2 as (  select x.batsman, x.non_striker, x.runs+ y.runs runs2 , x.runs runsx, y.runs runsy , x.ball2 ballx, y.ball2 bally from t1 x, t1 y where x.batsman=y.non_striker and x.non_striker=y.batsman )
			  select distinct case when ballx>bally then ballx else bally end ballz,
			  case when ballx>bally then batsman else non_striker end p1,
			  case when bally>ballx then batsman else non_striker end p2,
			  runs2 as runs, sum(runs2) over(order by case when ballx>bally then ballx else bally end)/2 score
			  from t2
			  order by ballz,p1,p2

-- Highest partnerships

with t1 as (            
            select match_id, inning, batsman,non_striker, max(cast(over||'.'||ball as decimal(7,3))) ball2, sum(total_runs) runs from ipl group by match_id, inning,batsman, non_striker),
          t2 as (  select x.match_id, x.batsman, x.non_striker, x.runs+ y.runs runs2 , x.runs runsx, y.runs runsy , x.ball2 ballx, y.ball2 bally from t1 x, t1 y 
          where x.match_id=y.match_id and x.batsman=y.non_striker and x.non_striker=y.batsman )
          select distinct match_id, case when ballx>bally then ballx else bally end ballz,
          case when ballx>bally then batsman else non_striker end p1,
          case when bally>ballx then batsman else non_striker end p2,
          runs2
          from t2
          order by runs2 desc

-- Hat trick across

with t1 as (select over, ball, match_id, inning, row_number() over(partition by match_id, inning order by  cast(over||'.'||ball as decimal(7,2) )) ball2,
         player_dismissed, bowler, dismissal_kind, cast(over||'.'||ball as decimal(7,2) ) over2

        from ipl --where player_dismissed is not null
       ),
       t2  as (select over2, ball2, match_id, inning, player_dismissed , dismissal_kind, bowler,
       case when  max(ball2) over(partition by match_id, inning,bowler order by ball2 rows between current row and 2 following) - ball2 <3
        then 1 else 0 end rw1
        from t1 where player_dismissed is not null ),

        t3 as ( select match_id, over2, ball2, player_dismissed , dismissal_kind, bowler, rw1,
        count(1) over(partition by match_id, inning, bowler, rw1) rw2 from t2 )
        select match_id, over2, ball2, player_dismissed , dismissal_kind, bowler, rw1, rw2 from t3 where rw2=3 and rw1=1
        
-- Highest number of centuries 

with t1 as (            
            select match_id, batsman,  sum(batsman_runs) runs from ipl group by match_id,batsman)
          select batsman, count(*) centuries, max(runs) highest from t1 where runs >= 100 group by batsman 
          order by centuries desc
-- Maximum runs in a over

with t1 as (            
            select match_id, batsman,  over, sum(total_runs) runs, count(*) as balls from ipl group by match_id,batsman, over)
          select * from t1 where  balls>=6 order by runs desc

-- Most maiden overs by a bowler

with t1 as (            
            select match_id, bowler,  over, sum(total_runs) runs, count(*) as balls from ipl group by match_id,bowler, over)
          select bowler, count(*) cw from t1 where runs=0 and balls>=6 group by bowler order by cw desc 

-- Most number of wickets

with t1 as (            
            select  bowler,  sum(case when player_dismissed is null then 0 else 1 end) wkts from ipl group by bowler )
          select bowler, wkts from  t1 order by wkts desc 
          
--More number of nuns

with t1 as (            
            select match_id, batsman,  sum(total_runs) runs from ipl group by match_id, batsman)
          select batsman, sum(runs) runs2 from t1 group by batsman order by runs2 desc 
          
-- Batsman who scored century by hitting 6          

with t1 as (            
            select match_id, batsman, batsman_runs, cast(over||'.'||ball as decimal(7,3)) over2,  sum(batsman_runs) over(partition by match_id, batsman order by cast(over||'.'||ball as decimal(7,3)) ) runs
            from ipl ),
        t2 as (
          select match_id, batsman, batsman_runs, runs,
          case when runs >= 100 and lag(runs) over(partition by match_id, batsman order by  over2 ) <100
          and batsman_runs =6
          then 1 else 0 end century_score
          from t1 )
          select * from t2 where century_score=1
          
-- Batsman dismissed after crossing the century    

        with t1 as (            
                    select match_id, batsman, batsman_runs, 
                    cast(over||'.'||ball as decimal(7,3)) over2,  sum(batsman_runs) over(partition by match_id, batsman order by cast(over||'.'||ball as decimal(7,3)) ) runs
                    from ipl ),
                    t11 as ( select match_id, batsman, player_dismissed from ipl where player_dismissed is not null ),
                t2 as (
                  select match_id, batsman, batsman_runs, runs, 
                  case when runs >= 100 and lag(runs) over(partition by match_id, batsman order by  over2 ) <100
                  and batsman_runs =6
                  then 1 else 0 end century_score
                  from t1 )
                  select * from t2 x
                  join t11 y on x.match_id=y.match_id and x.batsman=y.player_dismissed
                  where century_score=1
                  
-- Not out century batsmans

        with t1 as (            
                    select match_id, batsman, batsman_runs, player_dismissed, 
                    cast(over||'.'||ball as decimal(7,3)) over2,  sum(batsman_runs) over(partition by match_id, batsman order by cast(over||'.'||ball as decimal(7,3)) ) runs
                    from ipl ),
                    t11 as ( select match_id, batsman, player_dismissed from ipl where player_dismissed is not null ),
                t2 as (
                  select match_id, batsman, batsman_runs, runs, 
                  case when runs >= 100  then 1 else 0 end century_score
                  from t1 )
                  select match_id, batsman, max(runs) from t2 x
                  where 
                  century_score=1 and 
                  x.batsman not in ( select  player_dismissed from t11 p where p.match_id =x.match_id )
                  group by match_id, batsman
                    
                    
          
